# Chaos Engineering 

[Chaos Engineering](https://en.wikipedia.org/wiki/Chaos_engineering) describes the disciplicine of experimenting with a software system to ensure reliability and resiliency with unexpected conditions. Modern distributed architectures with microservices require chaos engineering frameworks and platforms to simulate production incidents and unexpected behaviour. 

Observability with [metrics](../metrics), [tracing](../tracing), [logs/events](../logs-events), etc. is needed to measure and observe the impact of chaos engineering. 

## Tools

### Litmus Chaos 

Litmus helps SREs and developers practice chaos engineering in a Cloud-native way.

- [Website](https://litmuschaos.io/)
- [Documentation](https://docs.litmuschaos.io/)

#### Facts

- Litmus is a [CNCF project](https://www.cncf.io/projects/litmus/), and was approved to [incubating in Jan 2022](https://www.cncf.io/blog/2022/01/11/litmuschaos-becomes-a-cncf-incubating-project/).

#### Hot Topics 

- [ChaosHub](https://hub.litmuschaos.io)